# README #

This README is intended to be used for providing guidance on the steps necessary to set up a new retailer on the SFTP platform for Cadillac Fairview.

### What is this repository for? ###

* Set up a new retailer on the current AWS SFTP transfer platform
* V0.1
* [Solution Overview](https://aws.amazon.com/blogs/storage/enable-password-authentication-for-aws-transfer-for-sftp-using-aws-secrets-manager/)

### How do I get set up? ###

* Open file sftp.tf and copy the block of code starting with `module` and change the strings **"sftp_test"** and **"testretailer"**
* Configuration
  
```
module "sftp_test" {
    source = "./modules"
    retailer = "testretailer"
    providers = {
        aws = aws.data-platform-non-prod
    }
}
```

* Deployment instructions
    * Commit the file sftp.tf to the master branch and the changes will be applied immediately
    * Login to AWS on the `data-platform-non-prod` account, go to the Secrets Manager service and find the secret created with the retailer name you just defined. Retrieve the secret values and look for the Password that was generated.
    * Use that password to login to the SFTP using the string below.

```
sftp testretailer@s-fca878c8aec04e59a.server.transfer.ca-central-1.amazonaws.com
```

### Who do I talk to? ###

* Diogo Zedan from the DevOps team
* Keerthi Penumutchu - Tech Lead from CF
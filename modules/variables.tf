variable "sftp_bucket" {
    type = string
    default = "cf-ravel-fileserver"
    description = "S3 bucket that's hosting the SFTP. Change if it's different from default"
}

variable "retailer" {
    type = string
    description = "Retailer that will be added on the SFTP server"
}


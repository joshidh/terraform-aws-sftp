terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "cadillacfairview"

    workspaces {
      name = "cf-sftp-retailers"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "ca-central-1"
}

provider "aws" {
  region = "ca-central-1"

  assume_role {
    role_arn = "arn:aws:iam::905376831911:role/OrganizationAccountAccessRole"
  }

  alias = "data-platform-non-prod"
}
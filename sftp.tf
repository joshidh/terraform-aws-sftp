# Start copying the block below
module "sftp_test" {
    source = "./modules"
    retailer = "testretailer"
    providers = {
        aws = aws.data-platform-non-prod
    }
}
# Copy until here and paste it below
# Change the strings sftp_test amd testretailer with the new retailer's name

module "sftp_retailer3" {
    source = "./modules"
    retailer = "retailer3"
    providers = {
        aws = aws.data-platform-non-prod
    }
}